// Chai is an assertion library
var expect = require('chai').expect;

// Package useful to run HTTP request on our server
var unirest = require('unirest')

// app is the module we want to test
var app = require('../server.js');


describe('Express server routes', function() {

    it('Get "/" should return status 200', function(done) {
        unirest.get('http://localhost:9000').end(function(res) {
            expect(res.status).to.equal(200);
            done();
        });
    });

    it('Get "/ios" should return status 200', function(done) {
        unirest.get('http://localhost:9000/ios').end(function(res) {
            expect(res.status).to.equal(200);
            done();
        });
    });

    it('Get "/android" should return status 200', function(done) {
        unirest.get('http://localhost:9000/android').end(function(res) {
            expect(res.status).to.equal(200);
            done();
        });
    });
});
