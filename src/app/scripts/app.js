require('marionette');

/**
 * The Marionnette.Application is a container for the rest of the code.
 * It is recommended that every Marionette app have at least one
 * instance of Application
 *
 * @namespace Client.App
 */
var App = Marionette.Application.extend({
    initialize: function(options) {
    }
});

// Create the app and set it up in the the app-container div in index.html
var app = new App({
    container: '.app-container'
});

// Add regions to the app
app.addRegions({
    mainContainer: app.container
});


module.exports = app;
