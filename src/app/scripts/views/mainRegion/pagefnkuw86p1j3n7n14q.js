var ChildItemView = require('../childItemView');


/**
 * Child Item View.
 *
 * @namespace Client.Views.childView
 */
module.exports = ChildItemView.extend({

    template: require('../../templates/mainRegion/pagefnkuw86p1j3n7n14q.hbs'),

    attributes: function() {
        return {
            'id': 'pagefnkuw86p1j3n7n14q',
            'class': 'page',
            'data-page': 'pagefnkuw86p1j3n7n14q'
        }
    },


    /**
     * View Render callback, allows to use method when your template
     * is rendered (e.g: Hide a loading GIF)
     *
     * @memberOf Client.Views.popup
     * @method onRender
     * @params {event}
     */
    onRender: function() {
        
        // YOTAKO Render View BEGIN
        // YOTAKO Render View END
    },


    /**
     * Initialize and display this view
     *
     * @memberOf Client.Views.childView
     * @constructs
     * @method initialize
     * @param options - e.g: Optional models
     */
    initialize: function() {

    }

});
