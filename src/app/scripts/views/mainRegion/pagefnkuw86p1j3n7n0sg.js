var Marionette = require('marionette');
var MainItemView = require('../mainItemView');

/**
 * Main View.
 *
 * @namespace Client.Views.mainView
 */
module.exports = MainItemView.extend({

    template: require('../../templates/mainRegion/pagefnkuw86p1j3n7n0sg.hbs'),

    attributes: function() {
        return {
            'id': 'pagefnkuw86p1j3n7n0sg',
            'class': 'page',
            'data-page': 'pagefnkuw86p1j3n7n0sg'
        }
    },


    /**
     * View Render callback, allows to use method when your template
     * is rendered (e.g: Hide a loading GIF)
     *
     * @memberOf Client.Views.popup
     * @method onRender
     * @params {event}
     */
    onRender: function() {
        
        // YOTAKO Render View BEGIN
        // YOTAKO Render View END
    },


    /**
     * Initialize and display this view
     *
     * @memberOf Client.Views.mainView
     * @constructs
     * @method initialize
     * @param options - e.g: Optional models
     */
    initialize: function(options) {

    }

});
