var Marionnette = require('marionette');


/**
 * Main Item View.
 *
 * @namespace Client.Views.mainItemView
 */
module.exports = Marionnette.ItemView.extend({

    originalEvents: {
        "click .navigate": "navigate"
    },

    additionalEvents: {
    },

    /**
     * Inherit and extends events from parent
     *
     * @memberOf Client.Views.mainIitemView
     * @method events
     */
    events : function() {
        return _.extend({},this.originalEvents,this.additionalEvents);
    },


    /**
     * Navigate to the URL called
     *
     * @memberOf Client.Views.mainItemView
     * @method navigate
     * @params {@event}
     */
    navigate: function(e) {
        window.router.navigate($(e.currentTarget).attr('href'), { trigger: true });

        e.preventDefault();
        return false;
    },


    /**
     * View Destroy() callback
     *
     * @method onDestroy
     * @memberOf Client.Views.mainItemView
     */
    onDestroy: function() {
        this.undelegateEvents();
    }
});
