var Marionnette = require('marionette');


/**
 * Popup View.
 *
 * @namespace Client.Views.popup
 */
module.exports = Marionnette.ItemView.extend({

    originalEvents: {
        "click .close": "close",
        "click .finish": "finish"
    },

    additionalEvents: {
    },


    /**
     * Inherit and extends events from parent
     *
     * @memberOf Client.Views.popup
     * @method events
     */
    events: function() {
        return _.extend({}, this.originalEvents, this.additionalEvents);
    },


    /**
     * Initialize and display this view
     *
     * @memberOf Client.Views.popup
     * @constructs
     * @method initialize
     * @param options - e.g: Optional models
     */
    initialize: function(options) {
        this.render();
    },


    /**
     * View Render callback, allows to use method when your template
     * is rendered (e.g: Hide a loading GIF)
     *
     * @memberOf Client.Views.popup
     * @method onRender
     * @params {event}
     */
    onRender: function() {
        window.f7.popup(this.template());
        this.setElement(this.template); // Reload events

        $('#' + this.id).on('closed', _.bind(this.onClosed, this));
    },


    /**
     * View Close() callback
     *
     * @memberOf Client.Views.popup
     * @method onClosed
     */
    onClosed: function() {
        this.destroy();
    },


    /**
     * View Destroy() callback
     *
     * @memberOf Client.Views.popup
     * @method onDestroy
     */
    onDestroy: function() {
        this.undelegateEvents();
    },


    /**
     * Call this.closePopup and preventDefault
     * @memberOf Client.Views.popup
     * @method close
     */
    close: function(e) {
        this.closePopup();

        e.preventDefault();
        return false;
    },


    /**
     * Call this.closePopup and preventDefault
     * @memberOf Client.Views.popup
     * @method finish
     */
    finish: function(e) {
        this.closePopup();

        e.preventDefault();
        return false;
    },


    /**
     * Close this popup ID, using Framework 7
     *
     * @memberOf Client.Views.popup
     * @method closePopup
     * @params {event}
     */
    closePopup: function(e) {
        window.f7.closeModal('#' + this.id);
    }
});