require('marionette');


var F7Region = Marionette.Region.extend({
    oldViews: null,
    initialize: function() {
        this.oldViews = [];
    },
    attachHtml: function(view) {
        this.$el.append(view.el);
    }
});


/**
 * Main layout of the application.
 *
 * @namespace Client.Views.mainLayoutView
 */
module.exports = Marionette.LayoutView.extend({

    template: require('../../templates/layouts/main.hbs'),

    attributes: function() {
        return {
            id: "mainLayout"
        }
    },

    events: {

    },

    // Describes region of this LayoutView
    regions: {

        mainView: {
            regionClass: F7Region,
            selector: ".view-main .pages"
        }
    },


    /**
     * Handles navigation between subviews of the layout
     *
     * @memberOf Client.Views.mainLayoutView
     * @method navigate
     * @params {@event}
     */
    navigate: function(e) {
        var view = $(e.currentTarget).data('view');

        // Switch between views
        if (view) {
            $('.view.active').removeClass('active');
            $(view).addClass('active');
        }

        if ($(e.currentTarget).attr('href') != "") {
            window.router.navigate($(e.currentTarget).attr('href'), {
                trigger: true
            });
        }

        e.preventDefault();
        return false;
    }
});
