var Marionnette = require('marionette');


/**
 * Child Item View.
 *
 * @namespace Client.Views.childItemView
 */
module.exports = Marionnette.ItemView.extend({

    // Default to current active view,
    // this is need to be set for popup's navigation
    activeView: '.view.active',


    originalEvents: {
        "click .navigate": "navigate",
        "click .back": "back"
    },


    additionalEvents: {
    	// events
    },


    /**
     * Set activeView options
     *
     * @memberOf Client.Views.childItemView
     * @constructs
     * @method initialize
     * @param options - e.g: Optional models
     */
    initialize: function(options) {
        if (options.activeView) {
            this.activeView = options.activeView;
        }
    },


    /**
     * Inherit and extends events from parent
     *
     * @memberOf Client.Views.childItemView
     * @method events
     */
    events : function() {
        return _.extend({},this.originalEvents,this.additionalEvents);
    },


    /**
     * Navigate to the URL called
     *
     * @memberOf Client.Views.childItemView
     * @method navigate
     * @params {event}
     */
    navigate: function(e) {
        window.router.navigate($(e.currentTarget).attr('href'), { trigger: true });

        e.preventDefault();
        return false;
    },


    /**
     * Back to the previous URL, call specific f7 router method back()
     *
     * @memberOf Client.Views.childItemView
     * @method back
     * @params {event}
     */
    back: function(e) {
        $(this.activeView)[0].f7View.router.back();

        e.preventDefault();
        return false;
    },


    /**
     * View Destroy() callback
     *
     * @memberOf Client.Views.childItemView
     * @method onDestroy
     */
    onDestroy: function() {
        this.undelegateEvents();
    },


    /**
     * On show event.
     *
     * @memberOf Client.Views.childItemView
     * @param  {view} view
     * @param  {region} region
     * @param  {load} load
     * @param  {f7view} f7view
     * @method onShow
     */
    onShow: function(view, region, load, f7view) {
        $(this.activeView)[0].f7View.router.load({
            content: this.el,
            animatePages: true
        });
    }
});
