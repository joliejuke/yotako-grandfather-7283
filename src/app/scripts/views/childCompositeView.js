var Marionnette = require('marionette');


/**
 * Child Composite View.
 *
 * @namespace Client.Views.childCompositeView
 */
module.exports = Marionnette.CompositeView.extend({
    // Default to current active view
    // this is need to be set for popup's navigation
    activeView: '.view.active',


    originalEvents: {
        "click .navigate": "navigate",
        "click .back": "back"
    },


    additionalEvents: {
    	// events
    },


    /**
     * Inherit and extends events from parent
     *
     * @memberOf Client.Views.childCompositeView
     * @method events
     */
    events : function() {
        return _.extend({},this.originalEvents,this.additionalEvents);
    },


    /**
     * Navigate to the URL called
     *
     * @memberOf Client.Views.childCompositeView
     * @method navigate
     * @params {event}
     */
    navigate: function(e) {
        window.router.navigate($(e.currentTarget).attr('href'), { trigger: true });

        e.preventDefault();
        return false;
    },


    /**
     * Back to the previous URL, call specific f7 router method back()
     *
     * @memberOf Client.Views.childCompositeView
     * @method back
     * @params {event}
     */
    back: function(e) {
        $(this.activeView)[0].f7View.router.back();

        e.preventDefault();

        return false;
    },


    /**
     * View Destroy() callback
     *
     * @memberOf Client.Views.childCompositeView
     * @method onDestroy
     */
    onDestroy: function() {
        this.undelegateEvents();
    },


    /**
     * Handles the 'onshow' event.
     *
     * @memberOf Client.Views.childCompositeView
     * @param  {view} view
     * @param  {region} region
     * @param  {load} load
     * @param  {f7view} f7view
     * @method onShow
     */
    onShow: function(view, region, load) {
        $(this.activeView)[0].f7View.router.load({
            content: this.el,
            animatePages: App.animatePages
        });
    }
});
