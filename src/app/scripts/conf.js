// This is ENV based conf
var conf = {
    dev: {
        api_host: ""
    },
    prod: {
        api_host: ""
    }
};

// This is Common conf
var common = {
    gcm: {
        senderID: ""
    }
};


/**
 * @memberOf Client.Config
 * @return config - Global config of application
 */
var mergeConf = function() {
    var app = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;
    var config;

    if (!app) {
        config = conf.dev;
    }
    else {
        config = conf.prod;
    }

    // Merge common config with env config
    config = $.extend({}, config, common);

    return config;
};


/**
 * @namespace Client.Config
 */
module.exports = mergeConf();
