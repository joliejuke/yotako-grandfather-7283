require('backbone');
require('marionette');

var App = require('App');
var MainLayout = require('./views/layouts/main');

// The following code is automatically managed by the framework.
// Your modifications will be pushed back to the mockups.
//
// VS NEW_REQUIRE_VIEW BEGIN
var Pagefnkuw86p1j3n7n0sgView = require('./views/mainRegion/pagefnkuw86p1j3n7n0sg');
var Pagefnkuw86p1j3n7n14qView = require('./views/mainRegion/pagefnkuw86p1j3n7n14q');
var Pagefnkuw86p1j3n7n1flView = require('./views/mainRegion/pagefnkuw86p1j3n7n1fl');
// VS NEW_REQUIRE_VIEW END


module.exports = Marionette.AppRouter.extend({
    layout: null,
    currentPage: null,

    initialize: function() {
        this.layout = new MainLayout();
        App.mainContainer.show(this.layout);

        // Delete old views when empty a region
        // ex : this.layout.REGIONNAME.on('empty', _.bind(this.clearOldViews, this));
    },

    clearOldViews: function(view, region, options) {
        // oldViews is a region's custom variable defined in views/layouts/main.js
        if (this.oldViews) {
            _.each(this.oldViews, function(view) {
                view.destroy();
            });
            this.oldViews = [];
        }
    },

    navigate: function (url) {
        // Disable changing URL
        Backbone.history.loadUrl(url);
    },

    /* Routes */

    // The following code is automatically managed by the framework.
    // Your modifications will be pushed back to the mockups.
    //
    // VS NEW_ROUTE BEGIN
    'artboard3': function() {
        var page = new Pagefnkuw86p1j3n7n0sgView;
        this.layout.mainView.show(page, {
            preventDestroy: true
        });
    },

    'artboard2': function() {
        var page = new Pagefnkuw86p1j3n7n14qView;
        this.layout.mainView.show(page, {
            preventDestroy: true
        });
    },

    'artboard1': function() {
        var page = new Pagefnkuw86p1j3n7n1flView;
        this.layout.mainView.show(page, {
            preventDestroy: true
        });
    },

    // VS NEW_ROUTE END


    routes: {
        // The following code is automatically managed by the framework.
        // Your modifications will be pushed back to the mockups.
        //
        // VS NEW_URL BEGIN
        'mainRegion/artboard3' : 'artboard3',
        '' : 'artboard3',
        'mainRegion/artboard2' : 'artboard2',
        'mainRegion/artboard1' : 'artboard1',
        // VS NEW_URL END
    }
});
