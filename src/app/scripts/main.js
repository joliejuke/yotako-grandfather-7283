var $ = require('jquery');
var Backbone = require('backbone');
var Framework7 = require('framework7');
var HandlebarsHelpers = require('./hbs_helpers');

var App = require('App');
var Conf = require('Conf');
var Router = require('./routes');

// Init app
App.on('start', function() {
    // Cross domain
    $.support.cors = true;

    // Attach router
    window.router = new Router();

    // Force first hashtag to prevent bug on # links
    if (window.location.href.indexOf('#') == -1) {
        window.location.href = "#";
    }

    // Init Framework7
    var f7 = new Framework7({
        router: true,
        pushState: false,
        fastClicks: true,
        scrollTopOnStatusbarClick: true
    });
    window.f7 = f7; // global framework7 instance

    /**
     * Register views (similar as NavigationController in iOS)
     */
    // Main view
    f7.addView('.view-main', {
        dynamicNavbar: true,
        domCache: true // Use to get deep back button work
    });

    // Attache current Marionette view to Framework7 page for delete
    f7.onPageInit('*', function(page) {
        var viewSelector = page.view.selector;

        // Save BM's view for destroy later
        switch(viewSelector) {
            case '.view-main':
                page.context = window.router.layout.mainView.currentView;
                break;

	    // Add other regions here
        }
    });

    /**
     * Destroy Marionette view after back
     */
    f7.onPageAfterBack('*', function(page) {
        page.context.destroy(); // On back, destroy the BM's view attach to the F7's Page
    });

    // Start Backbone history (not really used due to Framework7 router)
    if (Backbone.history){
        Backbone.history.start();
    }
});

// Start app
App.start();


/**
 * Cordova init
 */
if (window.cordova) {
    document.addEventListener('deviceready', function () {
        // On resume app
        document.addEventListener("resume", onResume, false);

        // On pause app
        document.addEventListener("pause", onPause, false);

        // Android : back / app closed
        document.addEventListener("backbutton", function() {
            // Close app if on first page of views (e.g. home, index...)
            if ($('.view.active').hasClass('view-main') && ($('.view-main .pages .page').length == 1))
            {
                exitApp();
            }
            // Back for all other views
            else {
		$('.view.active')[0].f7View.back();
            }
        }, false);
    });
}

// Exit app
function exitApp() {
    if (device.platform == 'Android') {
        navigator.app.exitApp();
    }
}

// On resume
function onResume() {

}

// On pause
function onPause() {

}
