var express = require('express');
var favicon = require('serve-favicon');


/**
 * Main NodeJS controller
 * @exports Express app
 * @namespace nodeController
 */
var app = express();
var PORT = process.env.PORT || 8080;


// Set the root of Express app
var appRoot = __dirname + '/app';
app.use(express.static(appRoot));


// Set favicon
app.use(favicon(__dirname + '/favicon.png'));


/**
 * GET root route : "/",
 *
 * @memberof nodeController
 * @method app.get
 * @param {string} route - String of the requested route
 * @return {object} Send HTTP status and html file
 */
app.get('/', function(req, res) {
    res.sendFile('ios.html', {root: appRoot});
});


/**
 * GET Android route : "/android",
 *
 * @memberof nodeController
 * @method app.get
 * @param {string} route - String of the requested route
 * @return {object} Send HTTP status and html file
 */
app.get('/android', function(req, res) {
	res.sendFile('android.html', {root: appRoot})
});


/**
 * GET Ios route : "/ios",
 *
 * @memberof nodeController
 * @method app.get
 * @param {string} route - String of the requested route
 * @return {object} Send HTTP status and html file
 */
app.get('/ios', function(req, res) {
	res.sendFile('ios.html', {root: appRoot})
});


/**
 * Application listen on defined PORT
 * Your local app can be reach at 'localhost:<PORT>'
 * PORT: default 9000
 * HEROKU: 80 (automatically set by Heroku)
 *
 * @memberof nodeController
 */
app.listen(PORT);
console.log('Magic happens on ' + PORT);


module.exports = app;
