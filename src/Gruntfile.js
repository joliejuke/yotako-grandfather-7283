'use strict';
var LIVERELOAD_PORT = 35729;
var SERVER_PORT = process.env.PORT || 8080;
var lrSnippet = require('connect-livereload')({ port: LIVERELOAD_PORT });

// # Globbing
module.exports = function(grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    // configurable paths
    var yeomanConfig = {
        app: './app',
        dist: '../app-mobile/www'
    };

    grunt.initConfig({
        yeoman: yeomanConfig,
        nodemon: {
            dev: {
                script: 'server.js'
            }
        },
        watch: {
            options: {
                nospawn: true,
                livereload: LIVERELOAD_PORT
            },
            sass: {
                files: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
                tasks: ['sass:server']
            },
            livereload: {
                options: {
                    livereload: grunt.option('livereloadport') || LIVERELOAD_PORT
                },
                files: [
                    '<%= yeoman.app %>/*.html',
                    '{.tmp,<%= yeoman.app %>}/styles/{,*/}*.css',
                    '{.tmp,<%= yeoman.app %>}/scripts/**/*.js',
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                    '<%= yeoman.app %>/scripts/templates/{,*/}*.{ejs,mustache,hbs}'
                ]
            },
            browserify: {
                files: [
                    '<%= yeoman.app %>/scripts/**/*.js',
                    '<%= yeoman.app %>/scripts/**/*.hbs'
                ],
                tasks: ['browserify:serve']
            }
        },
        clean: {
            dist: ['.tmp', '<%= yeoman.dist %>/*'],
            options: {
                force: true
            }
        },
        bower_concat: {
            ios: {
                include: [
                    'framework7'
                ],
                mainFiles: {
                    'framework7': 'dist/css/framework7.ios.min.css',
                },
                cssDest: '<%= yeoman.app %>/styles/ios.css'
            },
            android: {
                include: [
                    'framework7'
                ],
                mainFiles: {
                    'framework7': 'dist/css/framework7.material.min.css',
                },
                cssDest: '<%= yeoman.app %>/styles/android.css'
            },
            dist: {
                include: [
                    'framework7'
                ],
                mainFiles: {
                    'framework7': 'dist/css/framework7.ios.css'
                },
                cssDest: '<%= yeoman.dist %>/styles/bower.css'
            }
        },
        sass: {
            options: {
                loadPath: ['<%= yeoman.app %>/bower_components', './node_modules']
            },
            dist: {
                options: {
                    style: 'compressed'
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/styles',
                    src: ['*.{scss,sass}'],
                    dest: '<%= yeoman.dist %>/styles',
                    ext: '.min.css'
                }]
            },
            server: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/styles',
                    src: ['*.{css}'],
                    dest: '<%= yeoman.app %>/styles',
                    ext: '.css'
                }]
            }
        },
        browserify: {
            options: {
                alias: {
                    'App': '<%= yeoman.app %>/scripts/app.js',
                    'Conf': '<%= yeoman.app %>/scripts/conf.js'
                }
            },
            serve: {
                files: {
                    '<%= yeoman.app %>/bundle.js': ['<%= yeoman.app %>/scripts/**/*.js', '<%= yeoman.app %>/scripts/**/*.hbs']
                }
            }
        },
        useminPrepare: {
            html: '<%= yeoman.app %>/index.html',
            options: {
                dest: '<%= yeoman.dist %>'
            }
        },
        usemin: {
            html: ['<%= yeoman.dist %>/{,*/}*.html'],
            css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
            options: {
                dirs: ['<%= yeoman.dist %>']
            }
        },
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },
        htmlmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>',
                    src: '*.html',
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },
        uglify: {
            dist: {
                files: {
                    '<%= yeoman.dist %>/scripts/bundle.min.js': ['<%= yeoman.app %>/bundle.js'],
                    '<%= yeoman.dist %>/scripts/vendor/modernizr.min.js': ['<%= yeoman.app %>/bower_components/modernizr/modernizr.js']
                }
            }
        },
        copy: {
            dist: {
                files: [{
                    expand: true,
                    src: ['**'],
                    cwd: '<%= yeoman.app %>/fonts/',
                    dest: '<%= yeoman.dist %>/fonts/'
                }, {
                    expand: true,
                    src: ['**.svg'],
                    cwd: '<%= yeoman.app %>/images/',
                    dest: '<%= yeoman.dist %>/images/'
                }]
            }
        },
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= yeoman.dist %>/scripts/{,*/}*.js',
                        '<%= yeoman.dist %>/styles/{,*/}*.css',
                        '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                        '/styles/fonts/{,*/}*.*'
                    ]
                }
            }
        },
        shell: {
            prepare: {
                command: 'cd ../app-mobile/; cordova prepare'
            },
            runAndroid: {
                command: 'cd ../app-mobile/ & cordova build android & cordova run android --device'
            },
            runiOS: {
                command: 'cd ../app-mobile/; cordova build ios; cordova run ios --device'
            }
        },

        mochaTest: {
            test: {
                options: {
                    reporter: 'spec',
                },
                src: ['test/**/*.js']
            }
        },
        jsdoc: {
            dist: {
                src: [
                	'./app/scripts/*.js',
                	'./app/**/scripts/**/*.js',
                	'./server.js'
                	],
                dest: 'doc',
                options: {
                    destination: 'doc',
                    template: './node_modules/ink-docstrap/template',
                }
            }
        }
    });

	grunt.loadNpmTasks('grunt-jsdoc');

	grunt.registerTask('doc', [
        'jsdoc'
    ]);

    // Task used to display app in your browser
    // You can also easily deploy on Heroku servers
    grunt.registerTask('serve', function(target) {
        grunt.task.run([
            'browserify:serve',
            'bower_concat:android',
            'bower_concat:ios',
            'sass:server',
            'nodemon:dev'
        ]);
    });

    grunt.registerTask('build', function(target) {
        grunt.task.run([
            'clean:dist',
            'useminPrepare',
            'browserify:serve',
            'uglify:dist',
            'copy:dist',
            'bower_concat:dist',
            'sass:dist',
            'imagemin',
            'htmlmin',
            'usemin'
        ]);

        if (target == 'android') {
            grunt.task.run([
                'shell:runAndroid'
            ]);
        } else if (target == 'ios') {
            grunt.task.run([
                'shell:runiOS'
            ]);
        } else {
            grunt.task.run([
                'shell:prepare'
            ]);
        }
    });

    grunt.registerTask('test', [
        'mochaTest'
    ]);



    grunt.registerTask('default', [
        'serve'
    ]);






}
